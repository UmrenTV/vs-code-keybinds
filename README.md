# Custom Keybinds here include:  
- Toggle between editor and terminal (focus)  
- Add new file in VScode explorer  
- Add new folder in VScode explorer  
  
## Instructions + Location of Keybinds.json file on your VSCode:
- Press CTRL + SHIFT + P on your keyboard while in VSCode.
- Then type: Preferences: Open Keyboard Shortcuts - Pick the (JSON) option
- It should be empty, unless you've added some custom keybinds before.
- If you have something inside, and you know what it is, then you can copy the content to my file, or save it as backup for later.
- Now open the file keybindings.json you find on my bitbucket repo here, and either download it, or open it here. Select All and Copy. (Keybinds: Ctrl + A then Ctrl + C)
- Go to your file in VScode that you've opened before with CTRL SHIFT P (It should be named keybindings.json) and press these 3 shortcuts on your keyboard: Ctrl + A then Ctrl + V then Ctrl + S (Which basically does: Select All > Paste > Save File)
- gg.

### Nice keybind list from bradtraversy:
[Link to His Github](https://gist.github.com/bradtraversy/b28a0a361880141af928ada800a671d9)